package com.nf.pg.osms.services.employees.validation;

import com.nf.pg.osms.entities.Employee;
import com.nf.pg.osms.repositories.EmployeesRepository;
import com.nf.pg.osms.services.employees.EmployeesService;
import com.nf.pg.osms.services.employees.exceptions.EmployeeValidationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.time.LocalDate;
import java.util.regex.Pattern;

@Component
public class EmployeeValidatorImpl implements EmployeeValidator {

    private static final Pattern NAME_REGEXP = Pattern.compile("^[А-Яа-я-]+$");
    private static final Pattern PHONE_REGEXP = Pattern.compile("^(\\(?\\+?[0-9]*\\)?)?[0-9_\\- \\(\\)]*$");
    private static final Pattern EMAIL_REGEXP = Pattern.compile(
            "^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$");

    @Autowired
    private EmployeesService employeesService;

    @Autowired
    private EmployeesRepository repository;

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate(@Nonnull Employee employee) throws EmployeeValidationException {
        assertParamEmpty(employee.getFirstname(), "No first name specified.");
        assertParamEmpty(employee.getSurname(), "No surname specified.");
        assertParamEmpty(employee.getBirthdate(), "No birth date specified.");
        assertParamEmpty(employee.getEmployment(), "No employment date specified.");
        assertParamEmpty(employee.getSalary(), "No salary data specified.");
        assertParamEmpty(employee.getJob(), "No job specified.");
        assertParamEmpty(employee.getDepartment(), "No department specified.");
        assertParamEmpty(employee.getHead(), "No department head flag specified.");

        assertRussianCharacters(employee.getFirstname(), "firstname");
        assertRussianCharacters(employee.getSurname(), "surname");
        assertRussianCharacters(employee.getPatronymic(), "patronymic");

        assertEmploymentDate(employee);
        assertDismissionDate(employee);
        assertPhoneFormat(employee);
        assertEmailFormat(employee);

        assertSalary(employee);
    }

    private void assertSalary(Employee employee) throws  EmployeeValidationException {
        Employee currentBoss = repository.findEmployeeByDepartmentAndHead(employee.getDepartment(), true);
        if (employee.getSalary().compareTo(currentBoss.getSalary()) > 0) {
            throw new EmployeeValidationException("Salary of subordinate cannot be higher than his manager");
        }
    }

    private void assertPhoneFormat(Employee employee) throws EmployeeValidationException {
        String phone = StringUtils.trimToEmpty(employee.getPhone());
        if (phone.isEmpty()) {
            return;
        }

        if (!PHONE_REGEXP.matcher(phone).matches()) {
            throw new EmployeeValidationException("Invalid phone format.");
        }
    }

    private void assertEmailFormat(Employee employee) throws EmployeeValidationException {
        String email = StringUtils.trimToEmpty(employee.getEmail());
        if (email.isEmpty()) {
            return;
        }

        if (!EMAIL_REGEXP.matcher(email).matches()) {
            throw new EmployeeValidationException("Invalid email format.");
        }
    }

    private void assertEmploymentDate(Employee employee) throws EmployeeValidationException {
        LocalDate employment = employee.getEmployment();
        LocalDate birthdate = employee.getBirthdate();
        if (employment.isBefore(birthdate)) {
            throw new EmployeeValidationException("Employment date cannot be before birthday");
        }
    }

    private void assertDismissionDate(Employee employee) throws EmployeeValidationException {
        LocalDate employment = employee.getEmployment();
        LocalDate dismission = employee.getDismission();
        if (dismission != null && dismission.isBefore(employment)) {
            throw new EmployeeValidationException("Dismission date cannot be before employment date.");
        }
    }

    private void assertRussianCharacters(String name, String fieldname) throws EmployeeValidationException {
        if (name == null) {
            return;
        }

        if (!NAME_REGEXP.matcher(name).matches()) {
            throw new EmployeeValidationException(String.format("The field [%s] supports only Russian characters.",
                    fieldname));
        }
    }

    private void assertParamEmpty(String param, String msg) throws EmployeeValidationException {
        if (param.isEmpty()) {
            throw new EmployeeValidationException(msg);
        }
    }

    private void assertParamEmpty(Object param, String msg) throws EmployeeValidationException {
        if (param == null) {
            throw new EmployeeValidationException(msg);
        }
    }
}

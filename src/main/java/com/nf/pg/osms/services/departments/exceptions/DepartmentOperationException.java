package com.nf.pg.osms.services.departments.exceptions;

public class DepartmentOperationException extends Exception {

    public DepartmentOperationException(String message) {
        super(message);
    }
}

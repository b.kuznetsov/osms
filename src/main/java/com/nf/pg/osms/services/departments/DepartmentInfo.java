package com.nf.pg.osms.services.departments;

import com.nf.pg.osms.entities.Employee;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DepartmentInfo {

    private String name;
    private String founded;
    private Employee head;
    private Integer employeeCount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFounded() {
        return founded;
    }

    public void setFounded(String founded) {
        this.founded = founded;
    }

    public void setFounded(LocalDate date) {
        this.founded = (date == null)
                ? "Unknown"
                : date.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

    public Employee getHead() {
        return head;
    }

    public void setHead(Employee head) {
        this.head = head;
    }

    public Integer getEmployeeCount() {
        return employeeCount;
    }

    public void setEmployeeCount(Integer employeeCount) {
        this.employeeCount = employeeCount;
    }
}

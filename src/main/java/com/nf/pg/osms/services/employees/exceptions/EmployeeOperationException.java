package com.nf.pg.osms.services.employees.exceptions;

public class EmployeeOperationException extends Exception {

    public EmployeeOperationException(String message) {
        super(message);
    }

    public EmployeeOperationException(String message, Throwable cause) {
        super(message, cause);
    }
}

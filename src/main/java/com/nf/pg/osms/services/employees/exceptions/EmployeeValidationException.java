package com.nf.pg.osms.services.employees.exceptions;

public class EmployeeValidationException extends Exception {

    public EmployeeValidationException(String message) {
        super(message);
    }

}

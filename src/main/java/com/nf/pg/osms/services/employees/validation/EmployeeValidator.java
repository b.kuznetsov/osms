package com.nf.pg.osms.services.employees.validation;

import com.nf.pg.osms.entities.Employee;
import com.nf.pg.osms.services.common.Validator;
import com.nf.pg.osms.services.employees.exceptions.EmployeeValidationException;

public interface EmployeeValidator extends Validator<Employee, EmployeeValidationException> {

}

package com.nf.pg.osms.services.common;

import javax.annotation.Nonnull;

/**
 * Defines validator of something.
 *
 * @param <T> Type of object acceptable for validation.
 * @param <E> Exception type which will be thrown in case of invalid object.
 */
public interface Validator<T, E extends Exception> {

    /**
     * Validates correctness of the specified object.
     *
     * @param obj Something to validate.
     * @throws E If the specified object is not valid by any reasons.
     */
    void validate(@Nonnull T obj) throws E;

}

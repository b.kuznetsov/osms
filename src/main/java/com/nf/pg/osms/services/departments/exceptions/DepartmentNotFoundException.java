package com.nf.pg.osms.services.departments.exceptions;

public class DepartmentNotFoundException extends Exception {

    public DepartmentNotFoundException(long id) {
        super(String.format("No department with ID [%d]", id));
    }
}

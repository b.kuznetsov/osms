package com.nf.pg.osms.services.departments.validation;

import com.nf.pg.osms.entities.Department;
import com.nf.pg.osms.services.common.Validator;
import com.nf.pg.osms.services.departments.exceptions.DepartmentValidationException;

public interface DepartmentValidator extends Validator<Department, DepartmentValidationException> {

}

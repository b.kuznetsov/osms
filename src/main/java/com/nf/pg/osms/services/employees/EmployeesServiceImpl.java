package com.nf.pg.osms.services.employees;

import com.nf.pg.osms.entities.Department;
import com.nf.pg.osms.entities.Employee;
import com.nf.pg.osms.repositories.DepartmentsRepository;
import com.nf.pg.osms.repositories.EmployeesRepository;
import com.nf.pg.osms.services.departments.DepartmentsService;
import com.nf.pg.osms.services.departments.exceptions.DepartmentNotFoundException;
import com.nf.pg.osms.services.employees.exceptions.EmployeeNotFoundException;
import com.nf.pg.osms.services.employees.exceptions.EmployeeValidationException;
import com.nf.pg.osms.services.employees.validation.EmployeeValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeesServiceImpl implements EmployeesService {

    @Autowired
    private EmployeesRepository employeesRepository;

    @Autowired
    private DepartmentsService departmentsService;

    @Autowired
    private EmployeeValidator validator;

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public List<Employee> getAll() {
        return employeesRepository.findAll();
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Employee add(@Nonnull Employee employee) throws EmployeeValidationException {
        validator.validate(employee);
        employeesRepository.saveAndFlush(employee);
        return employee;
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Employee find(long id) throws EmployeeNotFoundException {
        return Optional.ofNullable(employeesRepository.findOne(id))
                .orElseThrow(() -> new EmployeeNotFoundException(id));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(long id) throws EmployeeNotFoundException {
        try {
            employeesRepository.delete(id);
        } catch (EmptyResultDataAccessException e) {
            throw new EmployeeNotFoundException(id);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Employee update(long id, @Nonnull Employee newData)
            throws EmployeeNotFoundException, EmployeeValidationException {

        Employee updatingEmp = find(id);
        updatingEmp.setFirstname(newData.getFirstname());
        updatingEmp.setSurname(newData.getSurname());
        updatingEmp.setBirthdate(newData.getBirthdate());
        updatingEmp.setEmployment(newData.getBirthdate());
        updatingEmp.setDismission(newData.getDismission());
        updatingEmp.setPhone(newData.getPhone());
        updatingEmp.setEmail(newData.getEmail());
        updatingEmp.setHead(newData.getHead());
        updatingEmp.setSalary(newData.getSalary());
        updatingEmp.setPatronymic(newData.getPatronymic());
        updatingEmp.setGender(newData.getGender());
        updatingEmp.setJob(newData.getJob());
        updatingEmp.setDepartment(newData.getDepartment());

        validator.validate(updatingEmp);
        return employeesRepository.saveAndFlush(updatingEmp);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public List<Employee> findByDepartment(long deptId) {
        try {
            Department dept = departmentsService.find(deptId);
            return employeesRepository.findEmployeesByDepartment(dept);
        } catch (DepartmentNotFoundException e) {
            return Collections.emptyList();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Employee dismissEmployee(long id, DismissionParams dismissionDate)
            throws EmployeeNotFoundException, EmployeeValidationException {

        Employee employee = find(id);
        employee.setDismission(dismissionDate.getDismissionDate());
        return update(id, employee);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Employee transferEmployee(long id, long deptId)
            throws EmployeeNotFoundException, DepartmentNotFoundException, EmployeeValidationException {

        Employee employee = find(id);
        Department department = departmentsService.find(deptId);
        employee.setDepartment(department);
        return update(id, employee);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public List<Employee> transferEmployees(long deptId, long newDeptId) throws DepartmentNotFoundException {
        Department department = departmentsService.find(deptId);
        Department newDept = departmentsService.find(newDeptId);
        List<Employee> employees = findByDepartment(deptId);
        employees.forEach(employee -> employee.setDepartment(newDept));
        List<Employee> saved = employeesRepository.save(employees);
        employeesRepository.flush();
        return saved;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Employee getManager(long empId) throws EmployeeNotFoundException {
        Employee employee = find(empId);
        Department dept = employee.getDepartment();
        return employeesRepository.findEmployeeByDepartmentAndHead(dept, true);
    }


}


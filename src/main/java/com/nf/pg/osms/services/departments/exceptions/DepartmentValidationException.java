package com.nf.pg.osms.services.departments.exceptions;

public class DepartmentValidationException extends Exception {

    public DepartmentValidationException(String message) {
        super(message);
    }
}

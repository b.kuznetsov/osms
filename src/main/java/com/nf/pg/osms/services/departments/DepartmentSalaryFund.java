package com.nf.pg.osms.services.departments;

import javax.annotation.Nonnull;
import java.math.BigDecimal;

public class DepartmentSalaryFund {

    private BigDecimal salaryFund;

    public DepartmentSalaryFund() {
    }

    public DepartmentSalaryFund(@Nonnull BigDecimal fund) {
        this.setSalaryFund(fund);
    }

    public BigDecimal getSalaryFund() {
        return salaryFund;
    }

    public void setSalaryFund(BigDecimal salaryFund) {
        this.salaryFund = salaryFund;
    }
}

package com.nf.pg.osms.services.departments;

import com.nf.pg.osms.entities.Department;
import com.nf.pg.osms.entities.Employee;
import com.nf.pg.osms.repositories.DepartmentsRepository;
import com.nf.pg.osms.repositories.EmployeesRepository;
import com.nf.pg.osms.services.departments.exceptions.DepartmentNotFoundException;
import com.nf.pg.osms.services.departments.exceptions.DepartmentOperationException;
import com.nf.pg.osms.services.departments.exceptions.DepartmentValidationException;
import com.nf.pg.osms.services.departments.validation.DepartmentValidator;
import com.nf.pg.osms.services.employees.EmployeesService;
import com.nf.pg.osms.services.employees.exceptions.EmployeeNotFoundException;
import com.nf.pg.osms.services.employees.exceptions.EmployeeValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class DepartmentsServiceImpl implements DepartmentsService {

    @Autowired
    private DepartmentsRepository departmentsRepository;

    @Autowired
    private EmployeesRepository employeesRepository;

    @Autowired
    private EmployeesService employeesService;

    @Autowired
    private DepartmentValidator validator;

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public List<Department> getAll() {
        return departmentsRepository.findAll();
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Department add(@Nonnull Department dept) throws DepartmentValidationException {
        validator.validate(dept);
        return departmentsRepository.saveAndFlush(dept);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove(long id) throws DepartmentNotFoundException, DepartmentOperationException {
        Department department = find(id);
        List<Employee> employeesInDept = employeesRepository.findEmployeesByDepartment(department);
        if (!employeesInDept.isEmpty()) {
            throw new DepartmentOperationException(String.format("Department (id=%d) is not empty", id));
        }
        departmentsRepository.delete(id);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Department update(long id, @Nonnull Department newData)
            throws DepartmentNotFoundException, DepartmentValidationException {

        Department updatingDept = find(id);

        updatingDept.setName(newData.getName());
        updatingDept.setParent(newData.getParent());
        updatingDept.setFounded(newData.getFounded());

        validator.validate(updatingDept);
        return departmentsRepository.save(updatingDept);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public DepartmentInfo getInfo(long id) throws DepartmentNotFoundException {
        Department department = find(id);
        DepartmentInfo info = new DepartmentInfo();
        info.setName(department.getName());
        info.setFounded(department.getFounded());
        info.setHead(employeesRepository.findEmployeeByDepartmentAndHead(department, true));
        info.setEmployeeCount(employeesRepository.findEmployeesByDepartment(department).size());
        return info;
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Department find(long id) throws DepartmentNotFoundException {
        return Optional.ofNullable(departmentsRepository.findOne(id))
                .orElseThrow(() -> new DepartmentNotFoundException(id));
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public List<Department> getSubDepartments(long parentId) throws DepartmentNotFoundException {
        Department parentDept = find(parentId);
        return departmentsRepository.findDepartmentByParent(parentDept);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Department moveDepartment(long id, long newParentId) throws DepartmentNotFoundException {
        Department department = find(id);
        Department parent = find(newParentId);
        department.setParent(parent);
        return departmentsRepository.saveAndFlush(department);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public List<Department> findDepartmentByName(String name) {
        return departmentsRepository.findDepartmentByNameContaining(name);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public DepartmentSalaryFund getSalaryFund(long id) throws DepartmentNotFoundException {
        List<Employee> employees = getEmployees(id);
        BigDecimal sum = employees.stream()
                .map(Employee::getSalary)
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
        return new DepartmentSalaryFund(sum);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public List<Employee> getEmployees(long deptId) throws DepartmentNotFoundException {
        Department department = find(deptId);
        return employeesRepository.findEmployeesByDepartment(department);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Employee createEmployeeUnderDepartment(long deptId, Employee employee)
            throws DepartmentNotFoundException, EmployeeValidationException {

        Department department = find(deptId);
        employee.setDepartment(department);
        return employeesService.add(employee);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Employee updateEmployeeUnderDepartment(long deptId, long empId, Employee employee)
            throws DepartmentNotFoundException, EmployeeNotFoundException, EmployeeValidationException {

        Department department = find(deptId);
        Employee empUnderDept = employeesRepository.findEmployeeByDepartmentAndId(department, empId);
        if (empUnderDept == null) {
            throw new EmployeeNotFoundException(empId);
        }

        empUnderDept.setFirstname(employee.getFirstname());
        empUnderDept.setSurname(employee.getSurname());
        empUnderDept.setBirthdate(employee.getBirthdate());
        empUnderDept.setEmployment(employee.getBirthdate());
        empUnderDept.setDismission(employee.getDismission());
        empUnderDept.setPhone(employee.getPhone());
        empUnderDept.setEmail(employee.getEmail());
        empUnderDept.setHead(employee.getHead());
        empUnderDept.setSalary(employee.getSalary());
        empUnderDept.setPatronymic(employee.getPatronymic());
        empUnderDept.setGender(employee.getGender());
        empUnderDept.setJob(employee.getJob());

        return employeesService.update(empId, empUnderDept);
    }
}

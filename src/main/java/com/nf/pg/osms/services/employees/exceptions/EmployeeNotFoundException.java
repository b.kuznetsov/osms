package com.nf.pg.osms.services.employees.exceptions;

public class EmployeeNotFoundException extends Exception {

    public EmployeeNotFoundException(long id) {
        super(String.format("No employee with ID [%d]", id));
    }

}

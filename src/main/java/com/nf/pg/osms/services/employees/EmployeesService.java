package com.nf.pg.osms.services.employees;

import com.nf.pg.osms.entities.Employee;
import com.nf.pg.osms.services.departments.exceptions.DepartmentNotFoundException;
import com.nf.pg.osms.services.employees.exceptions.EmployeeNotFoundException;
import com.nf.pg.osms.services.employees.exceptions.EmployeeValidationException;

import javax.annotation.Nonnull;
import java.time.LocalDate;
import java.util.List;

/**
 * Defines operations on employees.
 */
public interface EmployeesService {

    /**
     * Returns all available employees.
     *
     * @return List of employees.
     */
    @Nonnull
    List<Employee> getAll();

    /**
     * Finds employee with specified ID. If there is no such employee then empty optional will be returned.
     *
     * @param id Target employee id.
     * @return Optional with employee or empty.
     * @throws EmployeeNotFoundException If there is no employee with specified ID.
     */
    @Nonnull
    Employee find(long id) throws EmployeeNotFoundException;

    /**
     * Creates a new employee with specified parameters.
     *
     * @param employee Employee for adding.
     * @return Added employee.
     * @throws EmployeeValidationException If specified employee data is not valid.
     */
    @Nonnull
    Employee add(@Nonnull Employee employee) throws EmployeeValidationException;

    /**
     * Removes employee with specified ID.
     *
     * @param id Target employee ID.
     * @throws EmployeeNotFoundException If there is no employee with specified ID.
     */
    void remove(long id) throws EmployeeNotFoundException;

    /**
     * Updates employee with specified ID.
     *
     * @param id Target employee ID.
     * @param newData New data for updating.
     * @return Updated employee.
     * @throws EmployeeNotFoundException If there is no employee with such ID.
     * @throws EmployeeValidationException If specified data is not valid by some reasons.
     */
    @Nonnull
    Employee update(long id, @Nonnull Employee newData) throws EmployeeNotFoundException, EmployeeValidationException;

    /**
     * Returns employees from the department with specified ID.
     *
     * @param deptId Target department ID.
     * @return List of employees in the specified dept.
     */
    @Nonnull
    List<Employee> findByDepartment(long deptId);

    /**
     * Fires/dismisses employee.
     *
     * @param id Target employee ID.
     * @param dismissionDate Date of dismission.
     * @return Updated Employee.
     * @throws EmployeeNotFoundException If specified employee doesn't exist.
     * @throws EmployeeValidationException If specified date is not valid.
     */
    @Nonnull
    Employee dismissEmployee(long id, DismissionParams dismissionDate)
            throws EmployeeNotFoundException, EmployeeValidationException;

    /**
     * Moves employee to another department.
     *
     * @param id Target employee ID.
     * @param deptId Target department ID.
     * @return Updated employee.
     * @throws EmployeeNotFoundException If specified employee doesn't exist.
     * @throws EmployeeValidationException If something wrong with updated data.
     * @throws DepartmentNotFoundException If specified department doesn't exist.
     */
    @Nonnull
    Employee transferEmployee(long id, long deptId)
            throws EmployeeNotFoundException, DepartmentNotFoundException, EmployeeValidationException;

    @Nonnull
    List<Employee> transferEmployees(long deptId, long newDeptId) throws DepartmentNotFoundException;

    Employee getManager(long empId) throws EmployeeNotFoundException;
}

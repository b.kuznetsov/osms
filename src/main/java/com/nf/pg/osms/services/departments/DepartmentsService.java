package com.nf.pg.osms.services.departments;

import com.nf.pg.osms.entities.Department;
import com.nf.pg.osms.entities.Employee;
import com.nf.pg.osms.services.departments.exceptions.DepartmentNotFoundException;
import com.nf.pg.osms.services.departments.exceptions.DepartmentOperationException;
import com.nf.pg.osms.services.departments.exceptions.DepartmentValidationException;
import com.nf.pg.osms.services.employees.exceptions.EmployeeNotFoundException;
import com.nf.pg.osms.services.employees.exceptions.EmployeeValidationException;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Defines operations on departments.
 */
public interface DepartmentsService {

    /**
     * Returns all available departments.
     *
     * @return List of department instances.
     */
    @Nonnull
    List<Department> getAll();

    /**
     * Finds department by its id.
     *
     * @param id Id of target department.
     * @return Target department or empty one.
     * @throws DepartmentNotFoundException If such department doesn't exist.
     */
    @Nonnull
    Department find(long id) throws DepartmentNotFoundException;

    /**
     * Creates a new department.
     *
     * @param dept Parent department which owns the one.
     * @return Created department.
     * @throws DepartmentValidationException If specified Department contains invalid data.
     */
    @Nonnull
    Department add(@Nonnull Department dept) throws DepartmentValidationException;

    /**
     * Removes department with specified ID.
     *
     * @param id Id of target department.
     * @throws DepartmentNotFoundException If there is no such department.
     * @throws DepartmentOperationException If the department contains at least one employee.
     */
    void remove(long id) throws DepartmentNotFoundException, DepartmentOperationException;

    /**
     * Updates data of the department with specified ID.
     *
     * @param id Id of target department.
     * @param newData New data for applying.
     * @return Updated department.
     * @throws DepartmentNotFoundException If department with specified ID doesn't exist.
     * @throws DepartmentValidationException If specified data is not valid.
     */
    @Nonnull
    Department update(long id, @Nonnull Department newData)
            throws DepartmentNotFoundException, DepartmentValidationException;

    /**
     * Provides summary info about the department with specified ID.
     *
     * @param id ID of target department.
     * @return Department summary info structure.
     * @throws DepartmentNotFoundException If department with specified ID doesn't exist.
     */
    @Nonnull
    DepartmentInfo getInfo(long id) throws DepartmentNotFoundException;

    /**
     * Provides a list of subordinate departments.
     *
     * @param parentId ID of parent department.
     * @return List of departments.
     * @throws DepartmentNotFoundException If department with specified ID doesn't exist.
     */
    @Nonnull
    List<Department> getSubDepartments(long parentId) throws DepartmentNotFoundException;

    /**
     * Moves department to new parent department.
     *
     * @param id Department ID which is will be moved.
     * @param newParentId New parent department ID (destination).
     * @return Updated department.
     * @throws DepartmentNotFoundException If department (movable or parent) doesn't exist.
     */
    @Nonnull
    Department moveDepartment(long id, long newParentId) throws DepartmentNotFoundException;

    /**
     * Searches departments with by name.
     *
     * @param name Department search by name criteria.
     * @return List of departments with name like specified.
     */
    @Nonnull
    List<Department> findDepartmentByName(String name);

    /**
     * Provides info about salary fund of department with specified ID.
     *
     * @param id Target department ID.
     * @return Salary fund info.
     * @throws DepartmentNotFoundException If department with specified ID doesn't exist.
     */
    @Nonnull
    DepartmentSalaryFund getSalaryFund(long id) throws DepartmentNotFoundException;

    /**
     * Provides list of all employees in the department with specified ID (if it exists).
     *
     * @param deptId Target department ID.
     * @return List of employees.
     * @throws DepartmentNotFoundException If department with specified ID doesn't exist.
     */
    @Nonnull
    List<Employee> getEmployees(long deptId) throws DepartmentNotFoundException;

    /**
     * Creates employee under department with specified ID.
     *
     * @param deptId Target department ID.
     * @param employee Employee data.
     * @return Created employee.
     * @throws DepartmentNotFoundException If department with specified ID doesn't exist.
     */
    @Nonnull
    Employee createEmployeeUnderDepartment(long deptId, Employee employee) throws DepartmentNotFoundException, EmployeeValidationException;

    /**
     * Updates specific employee under specific department.
     * Department info will be ignored even if it will be specified.
     *
     * @param deptId Department ID.
     * @param empId Employee ID.
     * @param employee Employee data for update.
     * @return Updated employee.
     * @throws DepartmentNotFoundException If department with specified ID doesn't exist.
     * @throws EmployeeNotFoundException If employee with specified ID doesn't exist.
     */
    @Nonnull
    Employee updateEmployeeUnderDepartment(long deptId, long empId, Employee employee)
            throws DepartmentNotFoundException, EmployeeNotFoundException, EmployeeValidationException;
}

package com.nf.pg.osms.services.departments.validation;

import com.nf.pg.osms.entities.Department;
import com.nf.pg.osms.services.departments.exceptions.DepartmentValidationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;

@Component
public class DepartmentValidatorImpl implements DepartmentValidator {

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate(@Nonnull Department department) throws DepartmentValidationException {

        if (StringUtils.trimToEmpty(department.getName()).isEmpty()) {
            throw new DepartmentValidationException("Department name is not valid.");
        }

        if (department.getFounded() == null) {
            throw new DepartmentValidationException("Department foundation date is not valid");
        }
    }
}

package com.nf.pg.osms.services.employees;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public class DismissionParams {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate dismissionDate;

    public DismissionParams() {
    }

    public DismissionParams(LocalDate dismissionDate) {
        this.setDismissionDate(dismissionDate);
    }


    public LocalDate getDismissionDate() {
        return dismissionDate;
    }

    public void setDismissionDate(LocalDate dismissionDate) {
        this.dismissionDate = dismissionDate;
    }
}

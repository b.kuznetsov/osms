package com.nf.pg.osms.controllers.departments;

import com.nf.pg.osms.controllers.common.AbstractResponseEntityExceptionHandler;
import com.nf.pg.osms.controllers.common.RestError;
import com.nf.pg.osms.services.departments.exceptions.DepartmentNotFoundException;
import com.nf.pg.osms.services.departments.exceptions.DepartmentOperationException;
import com.nf.pg.osms.services.departments.exceptions.DepartmentValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice(basePackages = "com.nf.pg.osms.controllers.departments")
public class DepartmentsExceptionHandler extends AbstractResponseEntityExceptionHandler {

    @ExceptionHandler(DepartmentValidationException.class)
    protected ResponseEntity<Object> handleDeparmentValidationException(DepartmentValidationException e) {
        return buildResponseEntity(new RestError(HttpStatus.BAD_REQUEST, "Invalid Data", e));
    }

    @ExceptionHandler(DepartmentNotFoundException.class)
    protected ResponseEntity<Object> handleDepartmentNotFoundException(DepartmentNotFoundException e) {
        return buildResponseEntity(new RestError(HttpStatus.NOT_FOUND, "No Data", e));
    }

    @ExceptionHandler(DepartmentOperationException.class)
    protected ResponseEntity<Object> handleDepartmentOperationException(DepartmentOperationException e) {
        return buildResponseEntity(new RestError(HttpStatus.BAD_REQUEST, "Cannot perform operation", e));
    }
}

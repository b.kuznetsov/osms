package com.nf.pg.osms.controllers.departments;

import com.nf.pg.osms.entities.Department;
import com.nf.pg.osms.entities.Employee;
import com.nf.pg.osms.services.departments.DepartmentInfo;
import com.nf.pg.osms.services.departments.DepartmentSalaryFund;
import com.nf.pg.osms.services.departments.DepartmentsService;
import com.nf.pg.osms.services.departments.exceptions.DepartmentNotFoundException;
import com.nf.pg.osms.services.departments.exceptions.DepartmentOperationException;
import com.nf.pg.osms.services.departments.exceptions.DepartmentValidationException;
import com.nf.pg.osms.services.employees.EmployeesService;
import com.nf.pg.osms.services.employees.exceptions.EmployeeNotFoundException;
import com.nf.pg.osms.services.employees.exceptions.EmployeeValidationException;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = {"departments"})
@RestController
@RequestMapping(value = "/departments", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class DepartmentsController {

    @Autowired
    private DepartmentsService departmentsService;

    @Autowired
    private EmployeesService employeesService;

    @ApiOperation("Returns all available departments.")
    @RequestMapping(method = RequestMethod.GET, value = "/")
    public List<Department> getDepartments() {
        return departmentsService.getAll();
    }

    @ApiOperation("Returns department by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "No data.")})
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public Department findDepartment(@ApiParam(name = "id", required = true, value = "ID of an existing department.")
                                     @PathVariable("id") Long id)
            throws DepartmentNotFoundException {

        return departmentsService.find(id);
    }

    @ApiOperation("Creates a new department.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Invalid data.")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/")
    public Department addDepartment(@RequestBody Department dept) throws DepartmentValidationException {
        return departmentsService.add(dept);
    }

    @ApiOperation("Removes the department by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "No data.")})
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteDepartment(@ApiParam(name = "id", required = true, value = "ID of an existing department.")
                                 @PathVariable("id") Long id)
            throws DepartmentNotFoundException, DepartmentOperationException {

        departmentsService.remove(id);
    }

    @ApiOperation("Updates the department by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Invalid data."),
            @ApiResponse(code = 404, message = "No such department.")})
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public Department updateDepartment(@ApiParam(name = "id", required = true, value = "ID of an existing department.")
                                       @PathVariable("id") Long id,
                                       @ApiParam(required = true, value = "New data for updating.")
                                       @RequestBody Department newData)
            throws DepartmentValidationException, DepartmentNotFoundException {

        return departmentsService.update(id, newData);
    }

    @ApiOperation("Returns summary info about department by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "No such department.")})
    @RequestMapping(method = RequestMethod.GET, value = "/{id}/info")
    public DepartmentInfo getSummaryInfo(@ApiParam(name = "id", required = true, value = "ID of an existing department.")
                                         @PathVariable("id") Long id)
            throws DepartmentNotFoundException {

        return departmentsService.getInfo(id);
    }

    @ApiOperation("Returns dependent departments by parent ID.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "No such parent.")})
    @RequestMapping(method = RequestMethod.GET, value = "/{id}/sub")
    public List<Department> getDependentDepartments(@ApiParam(name = "id", required = true, value = "ID of an existing department.")
                                                    @PathVariable("id") Long id)
            throws DepartmentNotFoundException {

        return departmentsService.getSubDepartments(id);
    }

    @ApiOperation("Moves department to different parent.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "No such department or parent.")})
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}/move/{parentId}")
    public Department moveDepartment(@ApiParam(name = "id", required = true, value = "ID of an existing department for transfer.")
                                     @PathVariable("id") Long id,
                                     @ApiParam(name = "parentId", required = true, value = "ID of an existing destination department.")
                                     @PathVariable("parentId") Long parentId) throws
            DepartmentNotFoundException {

        return departmentsService.moveDepartment(id, parentId);
    }

    @ApiOperation("Searches department by name")
    @RequestMapping(method = RequestMethod.GET, value = "/search")
    public List<Department> findDepartmentByName(@ApiParam(name = "name", required = true, value = "Search by name criteria.")
                                                 @RequestParam(name = "name") String name) {
        return departmentsService.findDepartmentByName(name);
    }

    @ApiOperation("Calculates salary fund of the department with specified ID.")
    @RequestMapping(method = RequestMethod.GET, value = "/{id}/salary")
    public DepartmentSalaryFund getDepartmentSalaryFund(@ApiParam(name = "id", required = true, value = "ID of an existing department.")
                                                        @PathVariable("id") Long id)
            throws DepartmentNotFoundException {
        return departmentsService.getSalaryFund(id);
    }

    @ApiOperation("Returns list of employees attached to the department with specified ID")
    @RequestMapping(method = RequestMethod.GET, value = "/{id}/employees")
    public List<Employee> getEmployees(@ApiParam(name = "id", required = true, value = "ID of an existing department.")
                                           @PathVariable("id") Long id)
            throws DepartmentNotFoundException {
        return departmentsService.getEmployees(id);
    }

    @ApiOperation("Creates employee in the specified department.")
    @RequestMapping(method = RequestMethod.POST, value = "/{id}/employee")
    public Employee createDepartmentEmployee(@PathVariable("id") Long id, @RequestBody Employee employee) throws
            DepartmentNotFoundException, EmployeeValidationException {
        return departmentsService.createEmployeeUnderDepartment(id, employee);
    }

    @ApiOperation("Updates specified employee in the specified department.")
    @RequestMapping(method = RequestMethod.PUT, value = "/{deptId}/employee/{empId}")
    public Employee updateDepartmentEmployee(@PathVariable("deptId") Long deptId, @PathVariable("empId") Long empId,
                                             @RequestBody Employee newData)
            throws EmployeeValidationException, EmployeeNotFoundException, DepartmentNotFoundException {

        return departmentsService.updateEmployeeUnderDepartment(deptId, empId, newData);
    }

    @ApiOperation("Moves specified employees to the specified department.")
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}/employees/move")
    public List<Employee> transferEmployees(@PathVariable("id") Long id, @RequestParam("dept") Long dept)
            throws DepartmentNotFoundException {
        return employeesService.transferEmployees(id, dept);
    }
}
package com.nf.pg.osms.controllers.common;

import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

public abstract class AbstractResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    protected ResponseEntity<Object> buildResponseEntity(RestError error) {
        return new ResponseEntity<>(error, error.getHttpStatus());
    }

}

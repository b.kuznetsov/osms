package com.nf.pg.osms.controllers.employees;

import com.nf.pg.osms.controllers.common.AbstractResponseEntityExceptionHandler;
import com.nf.pg.osms.controllers.common.RestError;
import com.nf.pg.osms.services.employees.exceptions.EmployeeNotFoundException;
import com.nf.pg.osms.services.employees.exceptions.EmployeeOperationException;
import com.nf.pg.osms.services.employees.exceptions.EmployeeValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice(basePackages = "com.nf.pg.osms.controllers.employees")
public class EmployeesExceptionHandler extends AbstractResponseEntityExceptionHandler {

    @ExceptionHandler(EmployeeNotFoundException.class)
    private ResponseEntity<Object> handleEmployeeNotFoundException(EmployeeNotFoundException e) {
        return buildResponseEntity(new RestError(HttpStatus.NOT_FOUND, "No such employee", e));
    }

    @ExceptionHandler(EmployeeValidationException.class)
    private ResponseEntity<Object> handleEmployeeValidationException(EmployeeValidationException e) {
        return buildResponseEntity(new RestError(HttpStatus.BAD_REQUEST, "Invalid data", e));
    }

    @ExceptionHandler(EmployeeOperationException.class)
    protected ResponseEntity<Object> handleDepartmentOperationException(EmployeeOperationException e) {
        return buildResponseEntity(new RestError(HttpStatus.BAD_REQUEST, "Cannot perform operation", e));
    }
}

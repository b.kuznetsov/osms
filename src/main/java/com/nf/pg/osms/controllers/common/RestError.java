package com.nf.pg.osms.controllers.common;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.http.HttpStatus;

import javax.annotation.Nonnull;
import java.time.LocalDateTime;

public class RestError {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private LocalDateTime timestamp;
    private HttpStatus httpStatus;
    private String message;
    private String debugMessage;

    public RestError(@Nonnull HttpStatus status, @Nonnull String message, Throwable cause) {
        this.timestamp = LocalDateTime.now();
        this.httpStatus = status;
        this.message = message;
        this.debugMessage = cause.getLocalizedMessage();
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public String getDebugMessage() {
        return debugMessage;
    }
}

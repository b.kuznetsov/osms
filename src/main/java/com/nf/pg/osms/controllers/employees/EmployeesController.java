package com.nf.pg.osms.controllers.employees;

import com.nf.pg.osms.entities.Employee;
import com.nf.pg.osms.services.departments.exceptions.DepartmentNotFoundException;
import com.nf.pg.osms.services.employees.DismissionParams;
import com.nf.pg.osms.services.employees.EmployeesService;
import com.nf.pg.osms.services.employees.exceptions.EmployeeNotFoundException;
import com.nf.pg.osms.services.employees.exceptions.EmployeeValidationException;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "employees")
@RestController
@RequestMapping(value = "/employees", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class EmployeesController {

    @Autowired
    private EmployeesService employeesService;

    @ApiOperation("Returns all available employees.")
    @RequestMapping(method = RequestMethod.GET, value = "/")
    public List<Employee> getEmployees() {
        return employeesService.getAll();
    }

    @ApiOperation("Returns an employee by ID.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "No data.")})
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public Employee findEmployee(@ApiParam(name = "id", required = true, value = "ID of an existing employee.")
                                 @PathVariable("id") Long id)
            throws EmployeeNotFoundException {

        return employeesService.find(id);
    }

    @ApiOperation("Creates a new employee.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Invalid data.")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/")
    public Employee addEmployee(@ApiParam(required = true, value = "New employee data.")
                                @RequestBody Employee newEmployee)
            throws EmployeeValidationException {

        return employeesService.add(newEmployee);
    }

    @ApiOperation("Removes an employee by ID.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "No data.")})
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteEmployee(@ApiParam(name = "id", required = true, value = "ID of an existing employee.")
                               @PathVariable("id") Long id)
            throws EmployeeNotFoundException {

        employeesService.remove(id);
    }

    @ApiOperation("Updates an employee by ID.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Invalid data."),
            @ApiResponse(code = 404, message = "No data.")})
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public Employee updateEmployee(@ApiParam(name = "id", required = true, value = "ID of an existing employee.")
                                   @PathVariable("id") Long id,
                                   @ApiParam(required = true, value = "New data for updating.")
                                   @RequestBody Employee newData)
            throws EmployeeValidationException, EmployeeNotFoundException {

        return employeesService.update(id, newData);
    }

    @ApiOperation("Fires/dismisses employee with specified id.")
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}/dismiss")
    public Employee dismissEmployee(@ApiParam(name = "id", required = true, value = "ID of an existing employee.")
                                    @PathVariable("id") Long id,
                                    @ApiParam(required = true, value = "Dismission parameters.")
                                    @RequestBody DismissionParams dismissionParams)
            throws EmployeeValidationException, EmployeeNotFoundException {

        return employeesService.dismissEmployee(id, dismissionParams);
    }

    @ApiOperation("Moves employee to different department")
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}/move")
    public Employee transferEmployee(@PathVariable("id") Long id, @RequestParam("dept") Long deptId)
            throws EmployeeValidationException, EmployeeNotFoundException, DepartmentNotFoundException {

        return employeesService.transferEmployee(id, deptId);
    }

    @ApiOperation("Returns info about manager of specified employee.")
    @RequestMapping(method = RequestMethod.GET, value = "/{id}/manager")
    public Employee getManager(@PathVariable("id") Long id) throws EmployeeNotFoundException {
        return employeesService.getManager(id);
    }
}
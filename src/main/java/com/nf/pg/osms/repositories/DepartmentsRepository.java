package com.nf.pg.osms.repositories;

import com.nf.pg.osms.entities.Department;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DepartmentsRepository extends JpaRepository<Department, Long> {

    Department findDepartmentByName(String name);

    List<Department> findDepartmentByNameContaining(String name);

    List<Department> findDepartmentByParent(Department parent);
}

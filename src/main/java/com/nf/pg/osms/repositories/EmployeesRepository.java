package com.nf.pg.osms.repositories;

import com.nf.pg.osms.entities.Department;
import com.nf.pg.osms.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeesRepository extends JpaRepository<Employee, Long> {

    List<Employee> findEmployeesByDepartment(Department department);

    Employee findEmployeeByDepartmentAndId(Department department, Long empId);

    Employee findEmployeeByDepartmentAndHead(Department department, Boolean head);
}

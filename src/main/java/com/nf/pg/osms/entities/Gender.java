package com.nf.pg.osms.entities;

public enum Gender {
    UNSPECIFIED,
    MALE,
    FEMALE
}

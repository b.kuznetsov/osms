insert into departments(name) values
  ('Отдел кадров'),
  ('Администрация'),
  ('Бухгалтерия'),
  ('Отдел разработки'),
  ('Отдел технического обеспечения'),
  ('Отдел контроля качества');

insert into departments(name,parent) values (
  'Отдел автоматизации тестирования',
  (select departments.id from departments where departments.name = 'Отдел контроля качества'));

insert into jobs(name) values
  ('Инженер-программист'),
  ('Системный инженер'),
  ('Секретарь'),
  ('HR менеджер'),
  ('Системный администратор');

insert into employees (firstname, surname, birthdate, department, job, salary) values
  ('Иванов', 'Иван', '1990-01-01',
   (select departments.id from departments where departments.name = 'Отдел разработки'),
   (select jobs.id from jobs where jobs.name = 'Инженер-программист'), 20000),

  ('Петров', 'Петр', '1991-01-01',
   (select departments.id from departments where departments.name = 'Отдел технического обеспечения'),
   (select jobs.id from jobs where jobs.name = 'Системный инженер'), 10000),

  ('Сидоров', 'Сидор', '1992-02-02',
   (select departments.id from departments where departments.name = 'Отдел технического обеспечения'),
   (select jobs.id from jobs where jobs.name = 'Системный администратор'), 10000),

  ('Петров', 'Петр', '1993-03-03',
   (select departments.id from departments where departments.name = 'Администрация'),
   (select jobs.id from jobs where jobs.name = 'Секретарь'), 15000),

  ('Николаев', 'Николай', '1994-04-04',
   (select departments.id from departments where departments.name = 'Отдел разработки'),
   (select jobs.id from jobs where jobs.name = 'Инженер-программист'), 25000),

  ('Андреев', 'Андрей', '1994-04-23',
   (select departments.id from departments where departments.name = 'Отдел разработки'),
   (select jobs.id from jobs where jobs.name = 'Инженер-программист'), 25000),

  ('Иосифова', 'Мария', '1985-02-20',
   (select departments.id from departments where departments.name = 'Отдел кадров'),
   (select jobs.id from jobs where jobs.name = 'HR менеджер'), 25000);
create database osmsdb
  with
    owner = osms_user
    encoding = 'UTF8'
    connection limit = -1;

grant all on database osmsdb to osms_user;

create sequence osms_dept_id_seq
  increment 1
  start 1
  minvalue 1
  maxvalue 9223372036854775807
  cache 1;

alter sequence osms_dept_id_seq
  owner to osms_user;

create sequence osms_job_id_seq
  increment 1
  start 1
  minvalue 1
  maxvalue 9223372036854775807
  cache 1;

alter sequence osms_job_id_seq
  owner to osms_user;

create sequence osms_emp_id_seq
  increment 1
  start 1
  minvalue 1
  maxvalue 9223372036854775807
  cache 1;

alter sequence osms_emp_id_seq
  owner to osms_user;

create sequence osms_dept_log_id_seq
  increment 1
  start 1
  minvalue 1
  maxvalue 9223372036854775807
  cache 1;

alter sequence osms_dept_log_id_seq
  owner to osms_user;

create table departments
(
  id bigint default nextval('osms_dept_id_seq') primary key,
  founded date not null default current_date,
  name varchar(200) not null unique,
  parent bigint,
  constraint fk_self foreign key (parent) references departments(id)
    on update cascade
    on delete cascade
);

alter table departments
  owner to osms_user;

create table jobs
(
  id bigint default nextval('osms_job_id_seq') primary key,
  name varchar(50) not null unique
);

alter table jobs
  owner to osms_user;

create table employees
(
  id bigint default nextval('osms_emp_id_seq') primary key,
  firstname varchar(100) not null,
  surname varchar(100) not null,
  patronymic varchar(100),
  gender varchar(10),
  birthdate date not null,
  department bigint not null,
  job bigint not null,
  salary numeric(19,2) not null,
  employment date not null default current_date,
  dismission date,
  email varchar(100),
  phone varchar(15),
  head boolean not null default false,
  constraint fk_emp_job foreign key (job) references jobs(id)
    on update cascade
    on delete cascade,
  constraint fk_emp_dept foreign key (department) references departments(id)
    on update cascade
    on delete cascade
);

alter table employees
  owner to osms_user;


-- Department operation types
create type dept_op as enum
(
  'Создание',
  'Переименование',
  'Перемещение'
);

-- Departments log
create table dept_op_log (
  id bigint default nextval('osms_dept_log_id_seq') primary key,
  a_time timestamp not null default current_timestamp,
  action dept_op not null,
  message text
);

alter table dept_op_log
  owner to osms_user;

-- Funcrions for departments log update

-- Logging department creation
create or replace function log_dept_op_add() returns trigger as $$
begin
  insert into dept_op_log(action, message) values ('Создание', new.name);
  return new;
end;
$$ language plpgsql;

alter function log_dept_op_add()
owner to osms_user;

-- Logging department renaming
create or replace function log_dept_op_rename() returns trigger as $$
declare
  msg text;
begin
  msg := old.name || ' => ' || new.name;
  insert into dept_op_log(action, message) values ('Переименование', msg);
  return new;
end;
$$ language plpgsql;

alter function log_dept_op_rename()
owner to osms_user;

-- Logging department moving
create or replace function log_dept_op_move() returns trigger as $$
declare
  msg text;
  new_parent varchar(200);
  old_parent varchar(200);
begin
  if old.parent = null and new.parent = null then
    return new;
  end if;

  new_parent := (select name from departments where id = new.parent);
  if old.parent = null then
    msg := 'В => ' || new_parent;
  else
    old_parent = (select name from departments where id = old.parent);
    msg := old.name || ' => ' || new_parent;
  end if;
  insert into dept_op_log(action, message) values ('Перемещение', msg);
  return new;
end;
$$ language plpgsql;

alter function log_dept_op_move()
owner to osms_user;

-- Departments creation trigger
create trigger dept_create
after insert on departments
for each row
execute procedure log_dept_op_add();

-- Departments renaming trigger
create trigger dept_rename
after update on departments
for each row
when (old.name is distinct from new.name)
execute procedure log_dept_op_rename();

-- Departments moving trigger
create trigger dept_move
after update on departments
for each row
when (old.parent is distinct from new.parent)
execute procedure log_dept_op_move();

create or replace function set_emp_head() returns trigger as $$
begin
  if new.head = true then
    update employees
    set head = false
    where department = new.department and id <> new.id;
  end if;
  return new;
end;
$$ language plpgsql;

alter function set_emp_head()
owner to osms_user;

create trigger emp_head_create
after insert on employees
for each row
execute procedure set_emp_head();